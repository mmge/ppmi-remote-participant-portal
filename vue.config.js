module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  publicPath: "/ppmi/test/",
  devServer: {
    disableHostCheck: true,
    proxy: 'http://backend.test/',
  },
  css: {
    loaderOptions: {
      sass: {
        implementation: require('sass'),
      }
    }
  }
}