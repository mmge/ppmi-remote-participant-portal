import Vue from 'vue'
import Vuetify from 'vuetify'
//import theme from '@/plugins/theme'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const opts = {
  iconfont: 'mdi',
  
  theme: {
    themes: {
      light: {
        primary: "#E07800", //MJFF Orange
        secondary: '#263C49', //MJFF Dark Blue
        tertiary: '#495057',
        accent: '#3A91B7', //MJFF Light Blue
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
      },
      dark: {
        primary: "#E07800", //MJFF Orange
        secondary: '#263C49', //MJFF Dark Blue
        tertiary: '#495057',
        accent: '#3A91B7', //MJFF Light Blue
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
      }
    }
  }
}

export default new Vuetify(opts)