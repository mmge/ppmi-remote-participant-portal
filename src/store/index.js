import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSignaturePad from 'vue-signature-pad'

axios.defaults.baseURL = 'https://nform.iu.edu/dev/rsc/ppp_api/';

Vue.use(Vuex)
Vue.use(VueAxios, axios)
Vue.use(VueSignaturePad)

export default new Vuex.Store({
  state: {
    selected_participant: {},
    first_login: true,
    upsit_tasks: [],
    question_tasks: [],
    login_status: '',
    login_error: '',
    logged_out: false,
    token: localStorage.getItem('token') || '',
    user: localStorage.getItem('user') || ''
  },
  getters: {
    selected_participant: state => {
      if(Object.entries(state.selected_participant).length === 0 && state.selected_participant.constructor === Object) {
        return null;
      } else {      
        return state.selected_participant
      }
    },
    activeUser: state => state.user,
    upsit_tasks: state => state.upsit_tasks,
    question_tasks: state => state.question_tasks,
    comments: state => state.selected_participant.comments,
    topic: state => state.route.name,
    isLoggedIn: state => !!state.token,
    isLoggedOut: state => state.logged_out,
    authStatus: state => state.login_status,
    loginError: state => state.login_error,
  },
  mutations: {
    save_participant(state, participant) {
      Vue.set(state, 'selected_participant', participant);
    },
    save_upsit_tasks(state, tasks) {
      Vue.set(state, "upsit_tasks", tasks);
    },
    save_question_tasks(state, tasks) {
      Vue.set(state, "question_tasks", tasks);
    },
    auth_request(state) {
      state.login_status = 'loading';
    },
    auth_success(state, cred) {
        state.login_status = 'success';
        state.token = cred.token;
        state.user = cred.user;
        state.login_error = "";
    },
    auth_error(state, error) {
        state.login_status = 'error';
        state.login_error = error;
    },
    logout(state) {
        state.token = "";
        state.login_status = "";
        state.logged_out = true;
    },
  },
  actions: {
    login({commit}, creds) {
      return new Promise((resolve, reject) => {
          commit('auth_request');
          axios.post("/login", {user: creds.email, password: creds.password})
              .then(resp => {
                  const status = resp.data.status
                  if(status === 200) {
                    const token = resp.data.message.token
                    const user = resp.data.message.username
                    localStorage.setItem('user', user)               
                    localStorage.setItem('token', token)
                    axios.defaults.headers.common['Authorization'] = token
                    commit('auth_success', {token: token, user: user})   
                  } else {
                    commit('auth_error', resp.data.message)
                  }
                  resolve(resp)
              })
              .catch(err => {
                  commit('auth_error', err);
                  localStorage.removeItem('token');
                  localStorage.removeItem('user');
                  reject(err)
              })
      })
    },
    logout({commit, getters}) {
      return new Promise((resolve) => {
        if(getters.isLoggedIn) {
          commit('logout');
          localStorage.removeItem('token');
          localStorage.removeItem('user');
          delete axios.defaults.headers.common['Authorization'];
        }
        resolve();
      });
    },    
    get_participant({commit}, uuid) {
      axios.get("/participant",
        {params: {"uuid": uuid}}
      ).then(result => {
        commit('save_participant', result.data)
      }).catch(error => {
        throw new Error(`API ${error}`);
      })
    },
    get_upsit_tasks({commit}) {
      axios.get("/tasks/upsit").then(result => {
        commit('save_upsit_tasks', result.data)
      }).catch(error => {
        throw new Error(`API ${error}`);
      })
    },
    get_question_tasks({commit}) {
      axios.get("/tasks/question").then(result => {
        commit('save_upsit_tasks', result.data)
      }).catch(error => {
        throw new Error(`API ${error}`);
      })
    },
  },
  modules: {

  }
})
