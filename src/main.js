import Vue from 'vue'
import '@/components';
import vuetify from '@/plugins'
import { sync } from 'vuex-router-sync'

// Application imports
import App from './App'
import router from '@/router'
import store from '@/store'
sync(store, router)
import './registerServiceWorker'
import 'material-design-icons-iconfont/dist/material-design-icons.css';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
