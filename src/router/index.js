import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/'

Vue.use(VueRouter)

const routerOptions = [
  {path: '/', component: 'Home', name: "Home", meta: {title: 'PPMI Participant Portal', requiresAuth: true}},
  {path: '/landing', component: 'Landing', name: "Welcome", meta: {title: "Welcome to PPMI Participant Portal", requiresAuth: true}},
  {path: '/login', component: 'Login', name: "Login", meta: {title: 'Login - PPMI Participant Portal', requiresAuth: false}},
  {path: '/contact_info', component: 'ContactInfo', name: "Contact Info", meta: {title: 'Contact Info - PPMI Participant Portal', requiresAuth: true}},
  {path: '/consents', component: 'Consents', name: "Consent", meta: {title: 'Informed Consent - PPMI Participant Portal', requiresAuth: true}},
  {path: '/faqs', component: 'FAQs', name: "FAQs", meta: {title: 'FAQs - PPMI Participant Portal', requiresAuth: true}},
  {path: '/travel', component: 'Travel', name: "Travel Planning", meta: {title: 'Travel Planning - PPMI Participant Portal', requiresAuth: true}},
  {path: '/shipments', component: 'ShipmentTracking', name: "Shipment Tracking", meta: {title: 'Shipment Tracking - PPMI Participant Portal', requiresAuth: true}},
  {path: '/questionnaires', component: 'Questionnaire', name: "Questionnaire", meta: {title: 'Questionnaire - PPMI Participant Portal', requiresAuth: true}},
  {path: '/upsit', component: 'UPSIT', name: "UPSIT", meta: {title: 'UPSIT - PPMI Participant Portal', requiresAuth: true}},
  {path: '*', redirect: "/"},
]

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`../views/${route.component}.vue`)
  };
});

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {

  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      document.title = to.name + " - PPMI Participant Portal";
      next()
      return
    }
     
    next({
      path: '/login',
      params: {nextUrl: to.fullPath}
    })
  } else {
    document.title = to.name + " - PPMI Participant Portal";
    next()
  }

});


export default router
